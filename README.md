# Print from a webpage to Epson Thermal printers with ePOS-Print API

Minimal setup and code for demonstrating purposes of the ePOS-Print API.

![Image of Print Button](./print.png)
![Image of Epson tm m30](./epsontmm30.jpg)

[Epson ePOS-Print API](https://c4b.epson-biz.com/modules/community/index.php?content_id=3)

[ePOS-Print API User's Manual and Sample program](https://download.epson-biz.com/modules/pos/index.php?page=single_soft&cid=4237&pcat=52&pid=3272)