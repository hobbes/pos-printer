const printerURL = 'http://192.168.1.195/cgi-bin/epos/service.cgi',
    printButton = document.querySelector('#printButton'),
    deviceId = 'm30',
    timeout = 5000,
    requestPrintId = function () {
        let printJobId = 1;

        return function () {
            printJobId = printJobId + 1;
            return printJobId;
        };
    }(),
    request = (obj) => {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(obj.method || "GET", obj.url);
            xhr.timeout = obj.timeout || 0;
            if (obj.headers) {
                Object.keys(obj.headers).forEach(key => {
                    xhr.setRequestHeader(key, obj.headers[key]);
                });
            }
            xhr.onload = () => {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr.responseXML);
                } else {
                    reject(xhr.statusText);
                }
            };
            xhr.onerror = () => reject(xhr.statusText);
            xhr.ontimeout = () => reject(xhr.statusText);
            xhr.send(obj.body);
        });
    };

function createPrintRequestBody(deviceId, timeout, printJobId) {
    let data = '╔══════════════════════════════════════════════╗\n║Filmtitel Test wenn es mal länger wird und es ║\n╚══════════════════════════════════════════════╝\n┌──────────────────────────────────────────────┐\n│ Lorem Ipsum sdfg                             │\n└──────────────────────────────────────────────┘\n░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓████████████\n░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓████████████\n░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓████████████\n';

    return `
	<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
		<s:Header>
			<parameter xmlns="http://www.epson-pos.com/schemas/2011/03/epos-print">
				<devid>${deviceId}</devid>
				<timeout>${timeout}</timeout>
				<printjobid>${printJobId}</printjobid>
			</parameter>
		</s:Header>
		<s:Body>
			<epos-print xmlns="http://www.epson-pos.com/schemas/2011/03/epos-print">
				<text lang="en" linespc="0">${data}</text>
				<cut/>
			</epos-print>
		</s:Body>
	</s:Envelope>
	`;
}

function print(printJobId) {
    const printRequestBody = createPrintRequestBody(deviceId, timeout, printJobId);

    printButton.setAttribute("disabled", "");

    console.log(`id: ${printJobId} - print start`);
    return request({
        method: 'POST',
        url: printerURL,
        headers: {
            'Content-Type': 'text/xml; charset=utf-8',
            'If-Modified-Since': 'Thu, 01 Jan 1970 00:00:00 GMT',
            'SOAPAction': '""'
        },
        timeout: 3500,
        body: printRequestBody
    }).then((responseXML) => {
        let success = responseXML.getElementsByTagName('response')[0].getAttribute('success') === 'true',
            code = responseXML.getElementsByTagName('response')[0].getAttribute('code'),
            status = responseXML.getElementsByTagName('response')[0].getAttribute('status'),
            id = responseXML.getElementsByTagName('printjobid')[0].innerHTML,
            successMessage = success ? 'success' : 'failure';

        printButton.removeAttribute("disabled");
        console.log(`id: ${id} - print ${successMessage}`);
        console.log(`            code: ${code}`);
        console.log(`            status: ${status}`);
        console.log();
    }).catch((error) => {
        printButton.removeAttribute("disabled");
        console.log("print Network error occured. \n");
        console.log(`id: ${printJobId} - print failed`)
    });
}
printButton.addEventListener('click', function (event) {
    event.preventDefault();
    print(requestPrintId());
});
